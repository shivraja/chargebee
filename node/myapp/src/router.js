/**
 * Managing all route handlers
 */

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var authRouter = require("./routes/auth");
var eventsRouter = require("./routes/events");
var notificationRouter = require("./routes/notification");

module.exports = function(app) {
  app.use("/", indexRouter);
  app.use("/users", usersRouter);
  app.use("/auth", authRouter);
  app.use("/events", eventsRouter);
  app.use("/notification", notificationRouter);
};
