/**
 * Adding a new event
 * @param {*} req
 * @param {*} res
 */
function addEvent(req, res) {
  //TODO
}

/**
 * Updating an existing event
 * @param {*} req
 * @param {*} res
 */
function updateEvent(req, res) {
  //TODO
}

/**
 * Deleting an existing event
 * @param {*} req
 * @param {*} res
 */
function deleteEvent(req, res) {
  //TODO
}

/**
 * Adding new participant to an event
 * @param {*} req
 * @param {*} res
 */
function addParticipant(req, res) {
  //TODO
}

/**
 * Removing the participant of an event
 * @param {*} req
 * @param {*} res
 */
function removeParticipant(req, res) {
  //TODO
}

/**
 * Return all events that are valid, based on the duration and creation date
 * @param {*} req
 * @param {*} res
 */
function getAllEvents(req, res) {
  //TODO
}

/**
 * Return a particular event along with the participant details
 * @param {*} req
 * @param {*} res
 */
function getEvent(req, res) {
  //TODO
}

/**
 * This will be called when there is any info which needs to be notified
 */
function notifyUpdates() {
  //TODO
}

module.exports = {
  addEvent,
  updateEvent,
  deleteEvent,
  addParticipant,
  removeParticipant,
  getAllEvents,
  getEvent,
  notifyUpdates
};
