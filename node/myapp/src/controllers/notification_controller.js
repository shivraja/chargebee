/**
 * Gives the notification belonging to particular user
 * @param {*} req
 * @param {*} res
 */
function getNotifications(req, res) {}

/**
 * Adds the notification for a particular user,
 * This function will be called when there is any updation or deletion of event
 */
function addNotification(notification) {}

/**
 * Mark the notification as read for a particular user
 * @param {*} req
 * @param {*} res
 */
function updateNotificationStatus(req, res) {}

module.exports = {
  getNotifications,
  addNotification,
  updateNotificationStatus
};
