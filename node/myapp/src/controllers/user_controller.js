/**
 * Adding a new user
 * @param {*} req
 * @param {*} res
 */
function addUser(req, res) {
  // TODO
}

/**
 * Removing the user, first checking if the user is present and then removing
 * @param {*} req
 * @param {*} res
 */
function removeUser(req, res) {
  //TODO
}

/**
 * Checking if a particular user is present
 * @param {*} email
 */
function isUserPresent(email) {
  //TODO
}

module.exports = {
  addUser,
  removeUser,
  isUserPresent
};
