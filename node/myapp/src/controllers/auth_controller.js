/**
 * Checking if the user is valid based on the email and password provided
 * @param {*} req
 * @param {*} res
 */
function authenticateUser(req, res) {
  //TODO
}

/**
 * Registering a new user - First checking if the user is not present and then adding
 * @param {*} req
 * @param {*} res
 */
function registerUser(req, res) {
  //TODO
}

module.exports = {
  authenticateUser,
  registerUser
};
