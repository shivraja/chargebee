var mysql = require("mysql");
var dbConfig = require("../config/database_config.json");

var con = mysql.createConnection(dbConfig);

module.exports = con;
