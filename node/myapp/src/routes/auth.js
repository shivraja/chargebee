var express = require("express");
var router = express.Router();
var authController = require("../controllers/auth_controller");

/* Attaching the handler for endpoints */
router.get("/login", authController.authenticateUser);

router.post("/register", authController.registerUser);

module.exports = router;
