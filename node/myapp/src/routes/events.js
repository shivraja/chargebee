var express = require("express");
var router = express.Router();
var eventController = require("../controllers/event_controller");

/* Attaching the handler for endpoints */
router.get("/addEvent", eventController.addEvent);
router.post("/updateEvent", eventController.updateEvent);
router.delete("/deleteEvent", eventController.deleteEvent);
router.post("/addParticipant", eventController.addParticipant);
router.delete("/removeParticipant", eventController.removeParticipant);
router.get("/getAllEvents", eventController.getAllEvents);
router.get("/getEvent", eventController.getEvent);

module.exports = router;
