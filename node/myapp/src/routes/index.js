var express = require("express");
var router = express.Router();

/* Attaching the handler for endpoints */

/**
 * Handling the default route
 */
router.get("/", function(req, res, next) {
  console.log("Hello!!!");
  res.end("Hello ChargeBee!!!");
});

module.exports = router;
