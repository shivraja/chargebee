var express = require("express");
var router = express.Router();
var notificationController = require("../controllers/notification_controller");

/* Attaching the handler for endpoints */

router.get("/getNotifications", notificationController.getNotifications);
router.post(
  "/updateNotificationStatus",
  notificationController.updateNotificationStatus
);

module.exports = router;
