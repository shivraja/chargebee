var express = require("express");
var router = express.Router();
var userController = require("../controllers/user_controller");

/* Attaching the handler for endpoints */

router.post("/addUser", userController.addUser);
router.delete("/removeUser", userController.removeUser);

module.exports = router;
